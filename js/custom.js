$(function () {
    $(document).tooltip();
    $(document).on('click',  '.filter-toogle-options', function(e){
        $(this).toggleClass('open')
        $(this).prev().toggleClass('open')
    });
    // Открываем меню в адаптиве
    $(document).on('click', '.header .wrap-icon i', function(){
        $('.head-menu--mobile-modal').show()
        $('.header').addClass('modal')
    });
    /* --------------- */
    // Закроем меню
    $(document).on('click', function(e){
        $('body').css('overflow', 'scroll');
        if ($(e.target).closest('.header .wrap-icon i').length) {
            return
        } else if ($(e.target).closest('.header.modal .head, .wrapper-header-bottom-navigation, .wrap-phone').length) {
            return;
        } else {
            $('.head-menu--mobile-modal').hide()
            $('.header.modal').removeClass('modal')
        }
    });
    /* --------------- */
    // Выбор пункта самовывоза
    $(document).on('click', '.account__card-table td', function(){
        $(this).parent().find('td:last-child').html('Выбран')
    });
    /* --------------- */
    // Форма Характеристик
    $(document).on('click', '.product-details--description-title ul li', function(){
        let id = $(this).data('id')
        $(".product-details--description-title ul li.active").removeClass('active')
        $(this).toggleClass('active')
        $(".product__description.visible").removeClass('visible')
        $(".product__description[data-id='"+id+"']").toggleClass('visible')
    });
    /* --------------- */
    // Личный кабинет
    $(document).on('click', '.account-box ul li', function(){
        let id = $(this).data('id')
        $(".account-box ul li.active").removeClass('active')
        $(this).toggleClass('active')
        $(".product__description.visible").removeClass('visible')
        $(".product__description[data-id='"+id+"']").toggleClass('visible')
    });
    /* --------------- */
    // Откроем вкансию
    $(document).on('click', '.jobs__item', function(){        
        $(this).toggleClass('active');
        $(this).next().toggle("slide", 10);
    });
    /* --------------- */
    // Форма Поиска
    $(document).on('click', '.header-menu--box-icon.icon-search', function(){
        $(this).toggleClass('active-form');
        $(this).parents('.header-menu--box').addClass('opened');

        const input = $(this).parents('.header-menu--box').find("input#search");
        if(input.length){
            input.remove()
        } else {
            $(this).parents('.header-menu--box').prepend('<input id="search" name="search" type="text">');
        }
    });
    $(document).on('click', function(e){
        const $target = $(e.target);
        const $search = $('.header-menu--box-icon.active-form');
        const $box = $search.closest('.header-menu--box');

        if($search.length) {
            const $input = $box.find('input#search');

            if($target.hasClass('.header-menu--box-icon.active-form') || !$target.closest('.header-menu--box.opened').length) {
                $input.remove();
                $search.removeClass('active-form');
                $box.removeClass('opened');
            }
        }
    });
    /* ------------ */
    // Форма фильтра
    $(document).on('click', '.btn-accept-filter:submit', function(){
        
    });  
    $(document).on('click', '.btn-cancel-filter:reset', function(e){
        e.preventDefault();
        $(this).closest('form').find("input").each(function(i, v) {
            $(this).val("");
            $(this).prop('checked',false);
        });
    }); 
    /* ------------ */
    // Форма Сравнения
    $('.header-menu--box.compare--box').hover(function(){
        $('.wrap-hidden-modal-compare').show();
        $('.header-brands').hide();
        $('.header-submenu').hide();
    });
    $(document).on('click', function(e){
        if ($(e.target).hasClass('wrap-overlay-modal')) {
            $(e.target).parent().hide()           
        }
        if ($(e.target).hasClass('header-brands')) {
            $('.header-brands').hide()           
        }
        if ($(e.target).parents('.wrap-hidden-modal-compare')) {
            $('.wrap-hidden-modal-compare').hide()           
        }
    });
    /* ------------ */
    // Форма отзыва
    $(document).on('click', 'a.add-review', function(){
        $('.product__add--review').show()
        $(this).hide()
    });
    $(document).on('click', '.product__add--review .add-review', function(){
        $('.product__add--review').hide()
        $(this).hide()
        $('a.add-review').show()
    });

    // Форма добавления адреса доставки
    $(document).on('click', '.account__card-edit-modal', function(){
        $('.wrap-hidden-modal-delivery').show()
    });
    $(document).on('click', '.btn-close-modal-delivery', function(){
        $('.wrap-hidden-modal-delivery').hide()
    });
    $(document).on('click', '.account__card-edit-modal', function(){
        $('.wrap-hidden-modal-delivery').show()
    });
    $(document).on('click', '.btn-close-modal-delivery', function(){
        $('.wrap-hidden-modal-delivery').hide()
    });

    
    $(document).on('click', '.compare--delivery-basket', function(){
        $('.wrap-hidden-modal-delivery-basket').show()
    });
    $(document).on('click', '.btn-close-modal-delivery-basket', function(){
        $('.wrap-hidden-modal-delivery-basket').hide()
    });
    /* ------------ */
    // Подарок за подписку
    $(document).on('click', '.account__card-edit-gift', function(){
        $('.wrap-hidden-modal-gift').show()
    });
    $(document).on('click', '.btn-close-modal-gift', function(){
        $(this).parents('.wrap-hidden-modal-gift').addClass('gift-sudscribe')
        $(this).parents('.wrap-hidden-modal-gift').find('.wrap-overlay-modal').animate({
            left: '5%',
            bottom: '5%',
            top: '75%',
            height: '100px',
            width: '100px'
         });
    });
    $(document).on('click', '.wrap-hidden-modal-gift.gift-sudscribe', function(){
        $(this).removeClass('gift-sudscribe')
        $(this).find('.wrap-overlay-modal').animate({
            left: '0',
            bottom: '5%',
            top: '0',
            height: '100%',
            width: '100%'
         });
    });
    $(document).on('click', '.wrap-hidden-modal-gift .btn-reg', function(){
        $.ajax({
            type: "POST",
            url: "",
            data: $(this).parents('form').serialize(),
            success: function(msg){
                $(this).parents('.wrap-window-ckeck-in').html('Спасибо, мы скоро Вам ответим!')
                $('.wrap-hidden-modal-gift').delay(2000).hide()
            }
        });
        return false;
    });
    $(document).on('click', '.wrap-hidden-modal .btn-reg', function(e){
        $($(this).parents('form')).validate({
            rules: {
                surname: {
                    required: true,
                },
                name: {
                    required: true,
                },
                phone: {
                    required: true,
                    minlength: 10
                },
                middleName: {
                    required: true,
                },
                email: {
                    required: true,
                },
                description: {
                    required: true,
                },
                capcha: {
                    required: true,
                },
                password: {
                    required: true,
                }
            },
            //errorPlacement: function(error, element){ },  // Откючаем вывод ошибок
            messages: {
                surname: {
                    required: "Необходимо указать Фамилию",
                },
                name: {
                    required: "Необходимо указать Имя",
                },
                middleName: {
                    required: "Необходимо указать Отчество",
                },
                phone: {
                    required: "Необходимо указать телефон",
                },
                email: {
                    required: "Необходимо указать Email",
                },
                description: {
                    required: "Необходимо указать текст сообщения",
                },
                capcha: {
                    required: "Поле не должно быть пустым",
                },
                password: {
                    required: "Поле не должно быть пустым",
                }
            },
            submitHandler: function(form) {
                e.preventDefault(o)
                $.ajax({
                    url: form.action,
                    type: "POST",
                    data: form.serialize(),
                    success: function(response) {
                        $('#answers').html(response);
                    }
                });
            }
        });        
        /*let action = $(this).parents('form').attr(action)
        let data = $(this).parents('form').serialize()
        $.ajax({
            type: "POST",
            url: action,
            data: data,
            success: function(msg){
                $(this).parents('form').html('Спасибо, мы скоро Вам ответим!')
                $(this).parents('.wrap-hidden-modal').delay(2000).hide()
            }
        });*/
    });
    $(document).on('click', '.wrap-hidden-modal .btn-close', function(e){
        e.preventDefault()
        console.log
        $(this).parents('.wrap-hidden-modal').hide()
    });
    /* ------------ */
    $(document).on('click', '.section-sorting--grid-list', function(){
        $('.section-sorting--grid svg.active').removeClass('active')
        $(this).toggleClass('active')
        $('.big-content--boxs').toggleClass('catalogs-box--list')
    });
    $(document).on('click', '.section-sorting--grid-card', function(){
        $('.section-sorting--grid svg.active').removeClass('active')
        $(this).toggleClass('active')
        $('.big-content--boxs').removeClass('catalogs-box--list')
    });
    $("#range_price").slider({
        animate: "slow",
        range: true,
        max: 100000,
        slide : function(event, ui) {
            $(".range_price-min").val(ui.values[ 0 ]);
            $(".range_price-max").val(ui.values[ 1 ] );
        }
    });
    $(document).on('input', '.range_price-min', function() {
        $(this).val($(this).val().replace(/[A-Za-zА-Яа-яЁё]/, ''))
        $(this).parents('.filter--priсe').find('#range_price').slider("values", 0, $(this).val());
    });
    $(document).on('input', '.range_price-max', function() {        
        $(this).val($(this).val().replace(/[A-Za-zА-Яа-яЁё]/, ''))
        $(this).parents('.filter--priсe').find('#range_price').slider("values", 1, $(this).val());
    });   
    $(document).on('click', '.account__card-table-detail', function(){
        let id = $(this).data('order');
        let findOrder = $('tr[data-parent="'+id+'"')
        if(findOrder.length) {
            $(this).find('svg path:first-child').toggleClass('display-none')
            $('tr[data-parent="'+id+'"').toggleClass('visible-table')
        }
    });
    $(document).on('click', '.fa-calendar', function(){
        $(this).datepicker({
            closeText: 'Закрыть',
            prevText: 'Предыдущий',
            nextText: 'Следующий',
            currentText: 'Сегодня',
            monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
            monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн','Июл','Авг','Сен','Окт','Ноя','Дек'],
            dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
            dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
            dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
            weekHeader: 'Не',
            dateFormat: 'dd/mm/yy',
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: ''
        });
    });      
    $("#datepicker").datepicker({
        closeText: 'Закрыть',
        prevText: 'Предыдущий',
        nextText: 'Следующий',
        currentText: 'Сегодня',
        monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
        monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн','Июл','Авг','Сен','Окт','Ноя','Дек'],
        dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
        dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
        dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
        weekHeader: 'Не',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    });
    // Кастомный селект
    $(".custom-select").each(function() {
        var classes = $(this).attr("class"),
          id = $(this).attr("id"),
          name = $(this).attr("name");
        var template = '<div class="' + classes + '">';
        template +=
          '<span class="custom-select-trigger">' +
            $(this).find(':selected').html() +
          "</span>";
        template += '<div class="custom-options">';
        $(this)
          .find("option")
          .each(function() {
            template +=
              '<span class="custom-option ' +
              $(this).attr("class") +
              '" data-value="' +
              $(this).attr("value") +
              '">' +
              $(this).html() +
              "</span>";
          });
        template += "</div></div>";
      
        $(this).wrap('<div class="custom-select-wrapper"></div>');
        $(this).hide();
        $(this).after(template);
      });
      $(".custom-option:first-of-type").hover(
        function() {
          $(this)
            .parents(".custom-options")
            .addClass("option-hover");
        },
        function() {
          $(this)
            .parents(".custom-options")
            .removeClass("option-hover");
        }
      );
      $(".custom-select-trigger").on("click", function() {
        $("html").one("click", function() {
          $(".custom-select").removeClass("opened");
        });
        $(this)
          .parents(".custom-select")
          .toggleClass("opened");
        event.stopPropagation();
      });
      $(".custom-option").on("click", function() {
        $(this)
          .parents(".custom-select-wrapper")
          .find("select")
          .val($(this).data("value"));
        $(this)
          .parents(".custom-options")
          .find(".custom-option")
          .removeClass("selection");
        $(this).addClass("selection");
        $(this)
          .parents(".custom-select")
          .removeClass("opened");
        $(this)
          .parents(".custom-select")
          .find(".custom-select-trigger")
          .text($(this).text());
      });
      
    //Модальное окно РЕГИСТРАЦИИ
    $('.entrance_reg').on('click', function(e){
        e.preventDefault()
        $('.wrap-hidden-modal-registration').show();
        $('.wrap-hidden-modal').not('.wrap-hidden-modal-registration').hide();
        $('body').css('overflow', 'hidden');
    });
    $('.btn-close-modal-registration').on('click', function(){
        $('.wrap-hidden-modal-registration').hide();
        $('body').css('overflow', 'scroll');
    });
    //Модальное окно Входа
    $('.entrance').on('click', function(e){
        e.preventDefault()
        $('.wrap-hidden-modal-authorization').show();
        $('body').css('overflow', 'hidden');
    });    
    $('.btn-close-modal-authorization').on('click', function(){
        $('.wrap-hidden-modal-authorization').hide();
        $('body').css('overflow', 'hidden');
    });
    //Модальное окно восстановления пароля
    $('.authorization-lab').on('click', function(e){
        e.preventDefault()
        $('.wrap-hidden-modal-restore-pass').show();
        $('body').css('overflow', 'hidden');
        $('.wrap-hidden-modal').not('.wrap-hidden-modal-restore-pass').hide();
    });
    $('.btn-close-modal-restore-pass').on('click', function(){
        $('.wrap-hidden-modal-restore-pass').hide();
        $('body').css('overflow', 'hidden');
    });
    
    $('.card--picture-link').on('mouseover', function(){
        $(this).find('.card--quick-view').show();
        // 03.06. задача 35133, чек-лист - 247.
        // не вижу проблемы сделать отображение кнопки на css. Пока убрал строку вызывающую "багу".
        // $('body').css('overflow', 'hidden');
    });
    $('.card--picture-link').on('mouseout', function(){
        $(this).find('.card--quick-view').hide();
        // 03.06. задача 35133, чек-лист - 247.
        // не вижу проблемы сделать отображение кнопки на css. Пока убрал строку вызывающую "багу".
        // $('body').css('overflow', 'hidden');
    });
    //Модальное окно деталки
    $('.card--quick-view').on('click', function(e){
        e.preventDefault()
        $('.wrap-hidden-modal-tovar').show();
        $('.wrap-hidden-modal-tovar').find('.wrap-overlay-modal').show();
        $('body').css('overflow', 'hidden');
    });
    $('.btn-close-modal-tovar').on('click', function(){
        $('.wrap-hidden-modal-tovar').hide();
    }); 
    /* ----------- */
    //Модальное окно обратного звонка
    $('.wrap-phone--icon-feedback').on('click', function(e){
        e.preventDefault()
        $('.wrap-hidden-modal-feedback-call').show();
        $('.wrap-hidden-modal-feedback-call').find('.wrap-overlay-modal').show();
        $('body').css('overflow', 'hidden');
    });
    $('.btn-close-modal-feedback').on('click', function(){
        $('.wrap-hidden-modal-feedback-call').hide();
        $('body').css('overflow', 'scroll');
    });
    /* ----------- */
    //Модальное окно ПУНКТЫ САМОВЫВОЗА
    $('.card-box__item-pickup tr').on('click', function(e){
        e.preventDefault()
        $('.wrap-hidden-modal-delivery-basket').show();
        $('body').css('overflow', 'hidden');
    });
    $('.btn-close-modal-tovar').on('click', function(){
        $('.wrap-hidden-modal-delivery').hide();
        $('body').css('overflow', 'scroll');
    });
    /* ----------- */
    //Модальное окно пункта самовывоза
    // $('.wrap-phone--icon-delivery').on('click', function(e){
    //     e.preventDefault()
    //     $('.wrap-hidden-modal-delivery-basket').show();
    //     $('body').css('overflow', 'hidden');
    // });
    // $('.btn-close-modal-tovar').on('click', function(){
    //     $('.wrap-hidden-modal-delivery').hide();
    //     $('body').css('overflow', 'scroll');
    // });
    /* ----------- */

    $('.wrapper-header-bottom-navigation .icon-nav, .header-bottom-navigation li:first-child a').hover(function(){
        $('.header-submenu').show("slide", 10);
        $('.header-submenu--catalog').show();
        $('.header-submenu .wrap-hidden-modal').show();
        $('.wrap-hidden-modal-compare').hide();
        $('.header-brands').hide();
        $('body').css('overflow', 'scroll');
    });

    $('.header-bottom-navigation li:nth-child(2)').hover(function(){
        $('.header-brands').show("slide", 10);
        $('.wrap-hidden-modal-compare').hide();
        $('.header-submenu').hide();
        $('body').css('overflow', 'scroll');
    });    
    $(document).click( function(e){
        if ( $(e.target).closest('.header-submenu--catalog').length ) {
            return;
        }
        $('.header-submenu--catalog').hide();
        $('.header-submenu .wrap-hidden-modal').hide();
    });

    $('.header-submenu li .level.level1').hover(function(){
        $('.header-submenu .level2').addClass('invisible')
        $('.header-submenu .level2').css('height', $('.header-submenu--catalog').height())
        $('.header-submenu .level2 li').css('height', $('.header-submenu--catalog').height() / 2)
        if($(this).next('.level2').length) {
            $(this).next('.level2').removeClass('invisible')
        }
    });

    /* Левое меню */
    $('.wrap-left-sidebar ul div.flex').on('click', function(){
        let el0 = $(this)[0]
        if(el0) {
            el0 = $(el0)[0];
            let el = $(this).children("ul")
            if (el) { 
                $(el0).toggleClass('active')
                $(el0).next().toggleClass('active')
            }
        }
    });

    //----------------------header menu BEGIN----------------------
    var headerMenu = {};
    headerMenu.self = $('.wrapper-head-contacts');
    headerMenu.brands = $('.header-brands');
    headerMenu.submenu2 = $('.header-submenu');
    headerMenu.bottomBoxNav = $('.wrapper-header-bottom-navigation');
    headerMenu.wrapper = headerMenu.self.find('.head-contacts');
    headerMenu.topBox = headerMenu.wrapper.find('.header__top');
    headerMenu.bottomBox = headerMenu.wrapper.find('.header__bottom');
    headerMenu.items = headerMenu.self.find('.menu__item');
    headerMenu.subMenu = headerMenu.self.find('.sub-menu');
    headerMenu.addSubmenuOffsets = function() {
        this.subMenu.css({
            top:  230 - parseInt(headerMenu.bottomBox.css('padding-bottom')),
        });
    };
    headerMenu.addStickToTop = function () {
        var windowOffset = $(window).scrollTop();
        if(windowOffset > 50) {
            this.self.addClass('stick');
            this.bottomBoxNav.addClass('stick');
            this.submenu2.addClass('stick');
            this.brands.addClass('stick');

            this.wrapper.css({
                top: -(this.topBox.innerHeight()) ,
            });

            $('body').css({
                paddingTop: this.self.innerHeight(),
            });

            this.subMenu.css({
                top: this.bottomBox.innerHeight()
            });
            this.brands.css({
                top: 68 - parseInt(headerMenu.self.height()),
            });
            this.submenu2.css({
                marginTop: -52 - parseInt(headerMenu.self.height()),
            });
        } else {
            this.self.removeClass('stick');
            this.bottomBoxNav.removeClass('stick');
            this.submenu2.removeClass('stick');
            this.brands.removeClass('stick');

            this.self.css({
                top: '',
            });

            $('body').css({
                paddingTop: '',
            });
            this.brands.css({
                top: '',
            });
            this.submenu2.css({
                marginTop: '',
            });

            this.addSubmenuOffsets();
        }
    };
    headerMenu.init = function () {
        this.addSubmenuOffsets();
        this.addStickToTop();

        $(window).resize(function () {
            headerMenu.addSubmenuOffsets();
        });

        $(window).scroll(function () {
            headerMenu.addStickToTop();
        });
    };

    headerMenu.init();
    //----------------------header menu END----------------------



    //----------------------sliders BEGIN----------------------
    var sliderMainOptions = {
        items: 1,
        nav: true,
        dots: true,
        loop: true,
        navText: ['<div class="arrow-prev"><svg width="18" height="32" viewBox="0 0 18 32" fill="none" xmlns="http://www.w3.org/2000/svg">'+
        '<path d="M1 31L16 16L1 1" stroke="#A8A8A8" stroke-width="2"/>'+
        '</svg>'+
        '</div>','<div class="arrow-next"><svg width="18" height="32" viewBox="0 0 18 32" fill="none" xmlns="http://www.w3.org/2000/svg">'+
        '<path d="M1 31L16 16L1 1" stroke="#A8A8A8" stroke-width="2"/>'+
        '</svg>'+
        '</div>'],
        responsive:{
            0:{
                nav:false,
                loop:true
            },
            500:{
                nav: false,
                loop:true
            },
            1000:{
                nav:true,
                loop:true
            }
        }
    };

    var sliderMainCatalog = {
        items: 4,
        slidesToScroll: 1,
        nav: true,
        dots: false,
        loop: true,
        navText: ['<div class="arrow-prev"><svg width="18" height="32" viewBox="0 0 18 32" fill="none" xmlns="http://www.w3.org/2000/svg">'+
        '<path d="M1 31L16 16L1 1" stroke="#A8A8A8" stroke-width="2"/>'+
        '</svg>'+
        '</div>','<div class="arrow-next"><svg width="18" height="32" viewBox="0 0 18 32" fill="none" xmlns="http://www.w3.org/2000/svg">'+
        '<path d="M1 31L16 16L1 1" stroke="#A8A8A8" stroke-width="2"/>'+
        '</svg>'+
        '</div>'],
        responsiveClass:true,
        responsive:{
            0:{
                items:2,
                nav:false,
                loop:true
            },
            410:{
                items:2,
                nav: false,
                loop:true
            },
            800:{
                items:3,
                nav:true,
                loop:true
            },
            1000:{
                items:4,
                nav:true,
                loop:true
            }
        }
    };
    var sliderFoooterCatalog = {
        items: 4,
        slidesToScroll: 1,
        nav: true,
        dots: false,
        loop: true,
        navText: ['<div class="arrow-prev"><svg width="18" height="32" viewBox="0 0 18 32" fill="none" xmlns="http://www.w3.org/2000/svg">'+
        '<path d="M1 31L16 16L1 1" stroke="#A8A8A8" stroke-width="2"/>'+
        '</svg>'+
        '</div>','<div class="arrow-next"><svg width="18" height="32" viewBox="0 0 18 32" fill="none" xmlns="http://www.w3.org/2000/svg">'+
        '<path d="M1 31L16 16L1 1" stroke="#A8A8A8" stroke-width="2"/>'+
        '</svg>'+
        '</div>'],
        responsive:{
            0:{
                items:1,
                nav:false,
                loop: false,
                autoWidth: true,
                margin: 20,
            },
            450:{
                items:2,
                nav: false,
                loop: true,
                autoWidth: false
            },
            800:{
                items:3,
                nav:true,
                loop:true
            },
            1000:{
                items:4,
                nav:true,
                loop:true
            }
        }
    };
    var sliderTovar= {
        items: 6,
        slidesToScroll: 1,
        nav: true,
        dots: false,
        loop: true,
        navText: ['<div class="arrow-prev"><svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">'+
        '<path fill-rule="evenodd" clip-rule="evenodd" d="M15 30C23.2843 30 30 23.2843 30 15C30 6.71573 23.2843 0 15 0C6.71573 0 0 6.71573 0 15C0 23.2843 6.71573 30 15 30ZM15 28C22.1797 28 28 22.1797 28 15C28 7.8203 22.1797 2 15 2C7.8203 2 2 7.8203 2 15C2 22.1797 7.8203 28 15 28ZM13 21.6667L19.6667 15L13 8.33333V21.6667Z" fill="#2F2F37"/>'+
        '</svg>'+
        '</div>','<div class="arrow-next"><svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">'+
        '<path fill-rule="evenodd" clip-rule="evenodd" d="M15 30C23.2843 30 30 23.2843 30 15C30 6.71573 23.2843 0 15 0C6.71573 0 0 6.71573 0 15C0 23.2843 6.71573 30 15 30ZM15 28C22.1797 28 28 22.1797 28 15C28 7.8203 22.1797 2 15 2C7.8203 2 2 7.8203 2 15C2 22.1797 7.8203 28 15 28ZM13 21.6667L19.6667 15L13 8.33333V21.6667Z" fill="#2F2F37"/>'+
        '</svg>'+
        '</div>'],
        responsive:{
            0:{
                items:4,
                nav:true,
                loop:true
            },
            500:{
                items:4,
                nav:true,
                loop:true
            },
            800:{
                items:6,
                nav:true,
                loop:true
            },
            1000:{
                items:6,
                nav:true,
                loop:true
            }
        }
    };
    var sliderBigTovar = {
        items: 1,
        slidesToScroll: 1,
        nav: false,
        dots: false,
        loop: true,
    };
    var sliderBrends = {
        items: 6,
        slidesToScroll: 1,
        nav: true,
        dots: false,
        loop: true,
        navText: ['<div class="arrow-prev"><svg width="18" height="32" viewBox="0 0 18 32" fill="none" xmlns="http://www.w3.org/2000/svg">'+
        '<path d="M1 31L16 16L1 1" stroke="#A8A8A8" stroke-width="2"/>'+
        '</svg>'+
        '</div>','<div class="arrow-next"><svg width="18" height="32" viewBox="0 0 18 32" fill="none" xmlns="http://www.w3.org/2000/svg">'+
        '<path d="M1 31L16 16L1 1" stroke="#A8A8A8" stroke-width="2"/>'+
        '</svg>'+
        '</div>'],
        responsive:{
            0:{
                items:3,
                nav:false,
                dots: false,
                loop:true
            },
            500:{
                items:3,
                nav:true,
                dots: false,
                loop:true
            },
            800:{
                items:3,
                nav:true,
                dots: false,
                loop:true
            },
            1100:{
                items:5,
                nav:true,
                dots: false,
                loop:true
            },
            1400:{
                items:6,
                nav:true,
                dots: false,
                loop:true
            }
        }
    };
    var sliderBrends4 = {
        items: 4,
        slidesToScroll: 1,
        nav: true,
        dots: false,
        loop: true,
        navText: ['<div class="arrow-prev"><svg width="18" height="32" viewBox="0 0 18 32" fill="none" xmlns="http://www.w3.org/2000/svg">'+
        '<path d="M1 31L16 16L1 1" stroke="#A8A8A8" stroke-width="2"/>'+
        '</svg>'+
        '</div>','<div class="arrow-next"><svg width="18" height="32" viewBox="0 0 18 32" fill="none" xmlns="http://www.w3.org/2000/svg">'+
        '<path d="M1 31L16 16L1 1" stroke="#A8A8A8" stroke-width="2"/>'+
        '</svg>'+
        '</div>'],
        responsive:{
            0:{
                items:1,
                nav:false,
                loop:true
            },
            500:{
                items:2,
                nav:false,
                loop:true
            },
            800:{
                items:3,
                nav:false,
                loop:true
            },
            1000:{
                items:4,
                nav:true,
                loop:true
            }
        }
    };
    var sliderStocks = {
        items: 1,
        slidesToScroll: 1,
        nav: true,
        dots: false,
        loop: true,
        navText: ['<div class="arrow-prev"><svg width="18" height="32" viewBox="0 0 18 32" fill="none" xmlns="http://www.w3.org/2000/svg">'+
        '<path d="M1 31L16 16L1 1" stroke="#A8A8A8" stroke-width="2"/>'+
        '</svg>'+
        '</div>','<div class="arrow-next"><svg width="18" height="32" viewBox="0 0 18 32" fill="none" xmlns="http://www.w3.org/2000/svg">'+
        '<path d="M1 31L16 16L1 1" stroke="#A8A8A8" stroke-width="2"/>'+
        '</svg>'+
        '</div>'],
        responsive:{
            0:{
                items:1,
                nav:false,
                loop:true
            },
            700:{
                items:1,
                nav:true,
                loop:true
            }
        }
    };
    var sliderCompare = {
        items: 4,
        slidesToScroll: 1,
        nav: false,
        dots: false,
        loop: false,
        responsive:{
            0:{
                items:1,
                center: true,
            },
            400:{
                items:2,
                center: true,
            },
            700:{
                items:3,
                center: true,
            },
            1000:{
                items:4,
                center: true,
            },
            1025: {
                center: false,
            }
        }
    };
    if($('.slider__main').length) {
        $('.slider__main').owlCarousel(sliderMainOptions);
    }
    if($('.slider__catalog').length) {
        $('.slider__catalog').owlCarousel(sliderMainCatalog);
    } 
    if($('.slider__catalog--footer').length) {
        $('.slider__catalog--footer').owlCarousel(sliderFoooterCatalog);
    }
    if($('.big-slider__mobile').length) {
        $('.big-slider__mobile').owlCarousel(sliderBigTovar);


    }
    if($('.slider__catalog--tovar').length) {
        $('.slider__catalog--tovar').owlCarousel(sliderTovar);

        $('.slider__catalog--tovar .slider__item').on('click', function () {
            var $slide = $(this);
            var index = parseInt($slide.data('index'));

            $('.big-slider__mobile').length ? $('.big-slider__mobile').trigger('to.owl.carousel', [index,300,true]) : null;
        });

        $('.slider__catalog--tovar').on('change.owl.carousel', function(event) {
            $('.big-slider__mobile').length ? $('.big-slider__mobile').trigger('to.owl.carousel', [event.item.index,300,true]) : null;
        });

    }
    if($('.slider__brends').length) {
        $('.slider__brends').owlCarousel(sliderBrends);
    }
    if($('.slider__brends-4').length) {
        $('.slider__brends-4').owlCarousel(sliderBrends4);
    }
    if($('.slider__catalog--stocks').length) {
        $('.slider__catalog--stocks').owlCarousel(sliderStocks);
    }
    if($('.slider__compare').length) {
        $('.slider__compare').owlCarousel(sliderCompare);
    }


    //----------------------sliders END----------------------



    //----------------------parallax BEGIN----------------------
    if($('.parallax').length) {
        $('.banner__parallax').parallax();
    }
    //----------------------parallax END----------------------
});

// Добавление в корзину
$(document).on('click', '.btn-basket-item-pg-item.add_basket', function(){
    $('.wrap-hidden-modal-cart').show();
    const title = $('.product-details--title').html()
    const img = $('.wrap-card-item-main').css('background-image').replace(/url\((?:\"|\')?(.+)(?:\"|\')?\)/, '$1');
    $('.modal-body').html('<div><img src="'+img+'"></div>'
        +'<div>Товар <span>'+title+'</span> добавлен в Корзину</div>'
    );
    $('body').css('overflow', 'hidden');
}); 
$('.btn-close-modal-close').on('click', function(){
    $('.wrap-hidden-modal-cart').hide();
    $('body').css('overflow', 'hidden');
});
/* ------- */
$(document).on('click', '.badge-card--favor', function(){
    $('.wrap-hidden-modal-cart').show();
    const title = $(this).closest('.wrap-img-card').find('.card--name').text()
    const img = $(this).closest('.wrap-img-card').find('.card--picture img').attr('src');
    $('.modal-body').html('<div><img src="'+img+'"></div>'
        +'<div>Товар <span>'+title+'</span> добавлен в Избранное</div>'
    );
    $('body').css('overflow', 'hidden');

}); 
$(document).on('click', '.stepper__controls [spinner-button="down"]', function(){
    let value = $('.stepper__controls input').val()
    value = parseInt(value) - parseInt(1)
    value !== 0 ? $('.stepper__controls input').val(value) : ''
});  
$(document).on('click', '.stepper__controls [spinner-button="up"]', function(){
    let value = $('.stepper__controls input').val()
    let max = $('.stepper__controls input').attr('max')
    value++
    parseInt(max) !== value-1 ? $('.stepper__controls input').val(value++) : ''
}); 
    $('.account__card-table .rating').rating({
        readOnly: true,
        image: 'img/stars.png'
    });
    $('.product-rating').rating({
        readOnly: true,
        image: 'img/stars.png'
    }); 
    $('.rating').rating({
        readOnly: false,
        image: 'img/stars.png'
    });
    try {
        $("#datepicker").inputmask("99/99/9999",{ "placeholder": "дд/мм/гггг" });
        $('.mask-phone').inputmask("+7(999)999-99-99");
        $(".mask-email").inputmask("email");
        if($('#map').length) {
            ymaps.ready(function () {
                var myMap = new ymaps.Map('map', {
                        center: [43.035444, 44.666876],
                        zoom: 13.3
                    }, {
                        searchControlProvider: 'yandex#search'
                    }),

                    MyIconContentLayout = ymaps.templateLayoutFactory.createClass(
                        '<div style="color: #FFFFFF; font-weight: bold;">$[properties.iconContent]</div>'
                    ),

                    myPlacemarkWithContent = new ymaps.Placemark([43.045089, 44.656836], {
                        balloonContent: 'г. Владикавказ, ул. Джанаева, 38',
                    }, {
                        iconLayout: 'default#imageWithContent',
                        iconImageHref: 'img/icon_map.svg',
                        iconImageSize: [85, 85],
                        iconImageOffset: [-24, -24],
                        iconContentOffset: [15, 15],
                        iconContentLayout: MyIconContentLayout,
                        balloonCloseButton: true,
                        selectOnClick: false,
                        hideIconOnBalloonOpen: false,
                    }),

                    shop2 = new ymaps.Placemark([43.021307, 44.668936], {
                        balloonContent: 'г. Владикавказ, ул. Джанаева, 38',
                    }, {
                        iconLayout: 'default#imageWithContent',
                        iconImageHref: 'img/icon_map.svg',
                        iconImageSize: [85, 85],
                        iconImageOffset: [-24, -24],
                        iconContentOffset: [15, 15],
                        iconContentLayout: MyIconContentLayout,
                        balloonCloseButton: true,
                        selectOnClick: false,
                        hideIconOnBalloonOpen: false
                    }),

                    shop3 = new ymaps.Placemark([44.687979, 43.033283], {
                        balloonContent: 'г. Владикавказ, ул. Джанаева, 38',
                    }, {
                        iconLayout: 'default#imageWithContent',
                        iconImageHref: 'img/icon_map.svg',
                        iconImageSize: [85, 85],
                        iconImageOffset: [-24, -24],
                        iconContentOffset: [15, 15],
                        iconContentLayout: MyIconContentLayout,
                        balloonCloseButton: true,
                        selectOnClick: false,
                        hideIconOnBalloonOpen: false
                    }),

                    shop4 = new ymaps.Placemark([43.046264, 44.668899], {
                        balloonContent: 'г. Владикавказ, ул. Джанаева, 38',
                    }, {
                        iconLayout: 'default#imageWithContent',
                        iconImageHref: 'img/icon_map.svg',
                        iconImageSize: [85, 85],
                        iconImageOffset: [-24, -24],
                        iconContentOffset: [15, 15],
                        iconContentLayout: MyIconContentLayout,
                        balloonCloseButton: true,
                        selectOnClick: false,
                        hideIconOnBalloonOpen: false
                    }),

                    shop5 = new ymaps.Placemark([43.039333, 44.703761], {
                        balloonContent: 'г. Владикавказ, ул. Джанаева, 38g',
                    }, {
                        iconLayout: 'default#imageWithContent',
                        iconImageHref: 'img/hands.svg',
                        iconImageSize: [85, 85],
                        iconImageOffset: [-24, -24],
                        iconContentOffset: [15, 15],
                        iconContentLayout: MyIconContentLayout,
                        balloonCloseButton: true,
                        selectOnClick: false,
                        hideIconOnBalloonOpen: false
                    });
                myMap.geoObjects
                    .add(myPlacemarkWithContent)
                    .add(shop2)
                    .add(shop3)
                    .add(shop4)
                    .add(shop5)
            });
        }

        $("#zoom_mw").on('mouseover', function () {
            const $zoom = $(this);
            if($(window).innerWidth() > 1024) {
                if (!$zoom.hasClass('inited')) {
                    $zoom.addClass('inited');
                    $zoom.elevateZoom({scrollZoom: true});
                }
            } else {

                $('.zoomContainer').length ? $('.zoomContainer').remove() : null;
            }
        });
    } catch(e) {

    }
    //----------------------functions BEGIN----------------------
function makeSliderArrow(arrowClasses) {
    return  '<svg class="' + arrowClasses + '" width="14" height="22" viewBox="0 0 14 22" xmlns="http://www.w3.org/2000/svg">\n' +
                '<path d="M12.6606 21.0033C13.0331 20.6286 13.2422 20.1217 13.2422 19.5933C13.2422 19.0649 13.0331 18.558 12.6606 18.1833L5.5806 11.0033L12.6606 3.92331C13.0331 3.54859 13.2422 3.04169 13.2422 2.51331C13.2422 1.98494 13.0331 1.47804 12.6606 1.10332C12.4747 0.915858 12.2535 0.767069 12.0098 0.665532C11.766 0.563995 11.5046 0.511719 11.2406 0.511719C10.9766 0.511719 10.7152 0.563995 10.4714 0.665532C10.2277 0.767069 10.0065 0.915858 9.8206 1.10332L1.3406 9.58331C1.15314 9.76924 1.00435 9.99044 0.902815 10.2342C0.801279 10.4779 0.749001 10.7393 0.749001 11.0033C0.749001 11.2673 0.801279 11.5287 0.902815 11.7725C1.00435 12.0162 1.15314 12.2374 1.3406 12.4233L9.8206 21.0033C10.0065 21.1908 10.2277 21.3396 10.4714 21.4411C10.7152 21.5426 10.9766 21.5949 11.2406 21.5949C11.5046 21.5949 11.766 21.5426 12.0098 21.4411C12.2535 21.3396 12.4747 21.1908 12.6606 21.0033Z"/>\n' +
            '</svg>';

}
//----------------------functions END----------------------
 
//----------------------functions BEGIN----------------------\
    document.addEventListener('DOMContentLoaded', function() {


        var ellipsisText = function (e, etc) {
            var wordArray = e.innerHTML.split(" ");
            while (e.scrollHeight > e.offsetHeight) {
                wordArray.pop();
                e.innerHTML = wordArray.join(" ") + (etc || "...");
            }
        };
        [].forEach.call(document.querySelectorAll(".news__ellipsis"), function(elem) {
            ellipsisText(elem);
        });   

        var input = document.querySelector('.modal__search input');


        let searchDelete = document.querySelector('.seacrh__delete');
        if (searchDelete) {
            searchDelete.addEventListener('click',function(e){
                e.preventDefault();
                e.stopPropagation();

                let parent = this.parentElement;
                parent.querySelector('input').value = "";
            });
        }

        let cardOpen = document.querySelectorAll('.card--toggle');
        if (cardOpen) {
            for(let i = 0; i < cardOpen.length; i++){
                cardOpen[i].addEventListener('click',function(e){
                    e.preventDefault();
                    e.stopPropagation();;

                    let child = this.querySelector('.form-group');
                    child.classList.toggle('none');
                    this.classList.toggle('card__shadow');
                });
            }
        }


        let modalButtons = document.querySelector('.open-modal'),
        overlay = document.querySelector('.js-overlay-modal');
        if (modalButtons) {
            modalButtons.addEventListener('click', function(e) {
                e.preventDefault();
                e.stopPropagation()

                let modalId = this.getAttribute('data-modal'),
                modalElem = document.querySelector('.modal[data-modal="' + modalId + '"]');
                let size= 10;
                modalContent = modalElem.querySelector('.modal__content');
                modalDialog = modalElem.querySelector('.modal__dialog');
                modalContentP = modalElem.querySelector('.modal__content p');

                body.classList.add('hidden');
                modalElem.classList.add('show');
                overlay.classList.add('show');
            });

            buttonClose.addEventListener('click', function(e) {
                let parentModal = this.closest('.modal');
                body.classList.remove('hidden');
                parentModal.classList.remove('show');
                overlay.classList.remove('show');
            });
        }
    })
//----------------------functions END----------------------


$(function() {
    let size = 125,
    newsContent= $('.news__ellipsis');
    
    for (let i = 0; i < newsContent.length; i++) {
        let newsText = newsContent[i].innerHTML;
        if(newsText.length > size){
            newsContent[i].innerHTML = newsText.slice(0, size) + ' ...';
        }
    } 
});
(function() {
    const upDownBtn = document.querySelector('.up_down_btn');
    if(upDownBtn) {
        let check;
    
        function trackScroll() {
        let scrolled = window.pageYOffset;
        let coords = document.documentElement.clientHeight;
    
        if (scrolled > coords) {
            upDownBtn.classList.add('up_down_btn-show');
            upDownBtn.classList.remove('down');
            check = false;
        }
        if (scrolled === 0) {
            upDownBtn.classList.add('down');
            check = true;
            let paretq = document.documentElement
            let elemq = $('.footer__menu-blocks');
            //scrollToElement(elemq, paretq);
        }
        }

        function scrollToElement(element, parent) {
            $(parent)[0].scrollIntoView(true);
            $(parent).animate({
            scrollTop: $(parent).scrollTop() + $(element).offset().top - $(parent).offset().top
            }, {
            duration: 'slow',
            easing: 'swing'
            });
        }

        function backToTop() {
        upDownBtn.classList.add('up_down_btn-disabled');
        if (!check) {
            (function goTop() {  
            if (window.pageYOffset !== 0) {
                window.scrollBy(0, -80);
                setTimeout(goTop, 0);
            } else {
                upDownBtn.classList.remove('up_down_btn-disabled');
            }
    
            })();
            return;
    
        } else if (check) {
            (function goBottom() {
            var match = Math.ceil(window.pageYOffset + document.documentElement.clientHeight);
    
            if (match != document.documentElement.scrollHeight) {
                window.scrollBy(0, 80);
                setTimeout(goBottom, 0);
            } else {
                upDownBtn.classList.remove('up_down_btn-disabled');
            }
    
            })();
            return;
        }
    
        }
    
        window.addEventListener('scroll', trackScroll);
        upDownBtn.addEventListener('click', backToTop);
    }
})();

//добавляет стилизованный скролл в фильтр
$(function () {
    if($('.filter--category-box').length) {
        const filterScroll = new SimpleBar($('.filter--category-box')[0], {
            autoHide: false
        });
    }
});

//добавляет к <html> класс ie, на основании userAgent
$(function () {
    const ua = window.navigator.userAgent;
    const msie = ua.indexOf("MSIE ");
    const mozilla = ua.indexOf("Mozilla");

    msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./) ? $('html').addClass('ie') : null;
    mozilla !== -1 ? $('html').addClass('mozilla') : null;
});

$(function () {

    $.validator.addClassRules({
        'validate-email': {
            email: true,
            required: true,
        }
    });

    $('.js-form-validate').validate();

});

$(function () {
    $('.form-group .fa-calendar').on('click', function () {
        const $btn = $(this);
        const $box = $btn.closest('.wrap-inp-area');
        const $input = $box.find('.inp-area');

        $input.trigger('focus');
    });
});

$(function () {
    $('.product-volume__item').on('click', function () {
       var $btn = $(this);
       var $list = $btn.closest('.product-volume__list');
       var $btns = $list.find('.product-volume__item');

        $btns.removeClass('active');
        $btn.addClass('active');
    });
});

$(function () {
    $('.badge-card--favor').on('click', function () {
       $(this).hasClass('active') ? $(this).removeClass('active') : $(this).addClass('active');
    });

    $('.badge-card--bar').on('click', function () {
        $(this).hasClass('active') ? $(this).removeClass('active') : $(this).addClass('active');
    });
});

$(function () {
    var filterTooltip = {};
    filterTooltip.$container = $('.filter--category');
    filterTooltip.$checkboxes = filterTooltip.$container.find('input[type="checkbox"]');
    filterTooltip.TOOLTIP_CLASS = 'filter-tooltip';

    filterTooltip.init = function () {

        this.$checkboxes.on('change', function () {
            var self = filterTooltip;
            var STATIC_DATA = {
                count: 100,
                href: '#',
            };

            self.addTooltip(STATIC_DATA.count, STATIC_DATA.href);
            filterTooltip.addTimer(5000);
        });

    };

    filterTooltip.addTooltip = function (count, href) {
        var self = filterTooltip;
        var $tooltip = self.createTooltip(count, href);

        filterTooltip.removeTooltip();
        self.$container.append($tooltip);
    };

    filterTooltip.removeTooltip = function () {
        var self = filterTooltip;

        $('.' + self.TOOLTIP_CLASS).length ? $('.' + self.TOOLTIP_CLASS).remove() : null;
        typeof self.timerID === 'number' ? clearTimeout(self.timerID) : null;
    };

    filterTooltip.addTimer = function (delay) {
        var self = filterTooltip;
        var timerID = setTimeout(function () {
            filterTooltip.removeTooltip();
        }, delay);

        self.timerID = timerID;

        return timerID;
    };

    filterTooltip.createTooltip = function (count, href) {
        return $('<div class="' + filterTooltip.TOOLTIP_CLASS + '">' +
                    '<span class="' + filterTooltip.TOOLTIP_CLASS + '__text">Выбрано <span class="' + filterTooltip.TOOLTIP_CLASS + '__count">' + count + '</span> товаров.</span> ' +
                    '<a href="' + href + '" class="' + filterTooltip.TOOLTIP_CLASS + '__link">показать</a>' +
                '</div>');
    };

    filterTooltip.init();
});
